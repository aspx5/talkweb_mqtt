/*
 * Copyright (c) 2021-2021, talkweb 拓维信息 www.talkweb.com.cn.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.talkweb.iot.mqtt.broker;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * tw mqtt
 *
 * @author L.cm
 */
@SpringBootApplication
public class MqttBrokerApplication1 {

	public static void main(String[] args) {
		SpringApplication application = new SpringApplication(MqttBrokerApplication1.class);
		application.setAdditionalProfiles("dev1");
		application.run(args);
	}

}
