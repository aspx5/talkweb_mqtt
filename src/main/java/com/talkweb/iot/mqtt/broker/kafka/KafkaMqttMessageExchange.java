/*
 * Copyright (c) 2021-2021, talkweb 拓维信息 www.talkweb.com.cn.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *  http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.talkweb.iot.mqtt.broker.kafka;

import com.talkweb.iot.mqtt.broker.service.IMqttClusterService;
import net.dreamlu.iot.mqtt.core.server.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.annotation.KafkaListener;

/**
 * mqtt 集群消息处理
 *
 * @author L.cm
 */
@Configuration(proxyBeanMethods = false)
public class KafkaMqttMessageExchange {
	@Autowired
	private IMqttClusterService mqttClusterService;

	@KafkaListener(topics = KafkaTopics.TOPIC_MESSAGE_EXCHANGE)
	public void exchange(Message message) {
		if (message == null) {
			return;
		}
		mqttClusterService.exchange(message);
	}

	/**
	 * mqtt 下行广播消息
	 *
	 * @param message Message
	 */
	@KafkaListener(topics = KafkaTopics.TOPIC_MESSAGE_DOWN)
	public void downstream(Message message) {
		if (message == null) {
			return;
		}
		mqttClusterService.sendToClient(message);
	}

}
