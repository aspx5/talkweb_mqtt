# tw-mqtt 配置说明

| 配置项                               | 默认值           | 说明                                        |
| ------------------------------------ | ---------------- | ------------------------------------------- |
| mqtt.auth.enabled                    | true             | 是否启用 mqtt 认证，默认启用                    |
| mqtt.auth.username                   | twmqtt           | mqtt 连接认证 用户名                          |
| mqtt.auth.password                   | twmqtt           | mqtt 连接认证 密码                            |
| mqtt.server.node-name                | pid@ip:port      | 集群节点名，很重要，docker 下建议手动指定          |
| mqtt.server.name                     | Mica-Mqtt-Server | 名称                                        |
| mqtt.server.port                     | 1883             | 端口                                        |
| mqtt.server.ip                       | 0.0.0.0          | 服务端 ip 默认为空，0.0.0.0，建议不要设置   |
| mqtt.server.buffer-allocator         | 堆内存           | 堆内存和堆外内存                            |
| mqtt.server.heartbeat-timeout        | 1000 * 120       | 心跳超时时间(单位: 毫秒 默认: 1000 * 120)   |
| mqtt.server.read-buffer-size         | 8092             | 接收数据的 buffer size，默认：8092          |
| mqtt.server.max-bytes-in-message     | 8092             | 消息解析最大 bytes 长度，默认：8092         |
| mqtt.server.max-client-id-length     | 23               | mqtt 3.1 会校验此参数，其它协议版本不会     |
| mqtt.server.debug                    | false            | debug，如果开启 prometheus 指标收集建议关闭 |
| mqtt.server.web-port                 | 8083             | http、websocket 端口，默认：8083            |
| mqtt.server.websocket-enable         | true             | 开启 websocket 服务，默认：true             |
| mqtt.server.http-enable              | false            | 开启 http 服务，默认：true                  |
| mqtt.server.http-basic-auth.enable   | false            | 是否启用，默认：关闭                        |
| mqtt.server.http-basic-auth.password |                  | http Basic 认证密码                         |
| mqtt.server.http-basic-auth.username |                  | http Basic 认证账号                         |
